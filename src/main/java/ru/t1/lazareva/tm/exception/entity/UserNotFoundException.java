package ru.t1.lazareva.tm.exception.entity;

import ru.t1.lazareva.tm.exception.user.AbstractUserException;

public final class UserNotFoundException extends AbstractEntityException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}