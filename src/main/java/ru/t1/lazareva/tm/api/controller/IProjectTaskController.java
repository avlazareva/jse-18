package ru.t1.lazareva.tm.api.controller;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskFromProject();

}