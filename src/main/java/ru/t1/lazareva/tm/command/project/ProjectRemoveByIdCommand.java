package ru.t1.lazareva.tm.command.project;

import ru.t1.lazareva.tm.command.project.AbstractProjectCommand;
import ru.t1.lazareva.tm.model.Project;
import ru.t1.lazareva.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "project-remove-by-id";

    private static final String DESCRIPTION = "Remove project by id.";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        getProjectTaskService().removeProjectById(project.getId());
    }

}